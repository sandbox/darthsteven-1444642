<?php

class FeedsRSSFetcher extends FeedsHTTPFetcher {

  /**
   * Implements FeedsFetcher::fetch().
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);

    $state = $source->state(FEEDS_FETCH);
    $pages = array();
    if (!isset($state->pages)) {
      $state->pages = $this->listPages($source_config['source'], $source);
      $state->total = count($state->pages);
    }
    if (count($state->pages)) {
      $page = array_shift($state->pages);
      $state->progress($state->total, $state->total - count($state->pages));
      return new FeedsHTTPFetcherResult($page);
    }

    throw new Exception(t('Resource is not a file or it is an empty directory: %source', array('%source' => $source_config['source'])));
  }

  /**
   * Return an array of links in a feed.
   *
   * @param $url
   *   A stream wreapper URI that is a directory.
   * @param $source
   *   A FeedsSource.
   *
   * @return
   *   An array of URLs from the RSS feed.
   */
  protected function listPages($url, FeedsSource $source) {
    $poges = array();

    // Fetch an RSS feed.
    $feed = new FeedsHTTPFetcherResult($url);

    // Parse it for pages.
    $parser = new FeedsSyndicationParser();
    $result = $parser->parse($source, $feed);

    foreach ($result->items as $item) {
      if (isset($item['url']) && !empty($item['url'])) {
        $poges[] = $item['url'];
      }
    }

    return $poges;
  }
}
